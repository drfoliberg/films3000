package gestionDonnees.donnees.baseDonnees.structure.types;

public class VarChar extends Type{

	public VarChar(int precision) {
		super("VARCHAR", precision);
	}
	
}
