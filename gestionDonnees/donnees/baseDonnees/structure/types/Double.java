package gestionDonnees.donnees.baseDonnees.structure.types;

public class Double extends Type {

	public Double(int precision) {
		super("DOUBLE",precision);
	}
	
}
