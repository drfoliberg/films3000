package gestionDonnees.donnees.baseDonnees.structure.types;

public class Char extends Type {

	public Char(int precision) {
		super("CHAR", precision);
	}

}
