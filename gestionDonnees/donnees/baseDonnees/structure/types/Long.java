package gestionDonnees.donnees.baseDonnees.structure.types;

public class Long extends Type {

	public Long() {
		super("BIGINT");
	}

}
